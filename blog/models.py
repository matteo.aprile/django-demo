from django.conf import settings 
from django.db import models 
from django.utils import timezone 

# definizione del blog post
class Post(models.Model):
    # model dell'autore con specifica funzione nel momenot del delete
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    # model per definire un limite di lunchezza del titolo
    title = models.CharField(max_length=200)

    # model per definire un limite illimitato per il testo del post
    text = models.TextField()

    # model per la data di creazione/publicazione
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title