from django.urls import path
from . import views

urlpatterns = [
    path('', views.post_list, name='post_list'), # imposto http://127.0.0.1:8000/ come home page dato che lascio vuoto il primo parametro
    path('post/<int:pk>/', views.post_detail, name='post_detail'),
    path('post/new/', views.post_new, name='post_new'),
    path('post/<int:pk>/edit/', views.post_edit, name='post_edit'),
]