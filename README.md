# django-demo

## creazione del progetto
```
$ python -m pip install Django
$ django-admin startproject djangoSite .
```

### modifiche apportate a `djangoSite/settings.py`
```
TIME_ZONE = 'Europe/Rome'
STATIC_URL = '/static/'
STATIC_ROOT = BASE_DIR / 'static'
ALLOWED_HOSTS = ['127.0.0.1', '.pythonanywhere.com']
```

### creazione del db
```
$ python manage.py migrate
```

### run del server
```
$ python manage.py runserve
Watching for file changes with StatReloader
Performing system checks...

System check identified no issues (0 silenced).
May 24, 2023 - 08:21:19
Django version 4.2.1, using settings 'djangoSite.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.
```

## setup di un blog
```
$ python manage.py startapp blog
```
dire a django di usare il blog scrivendo in `djangoSite/settings.py`:
```
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'blog.apps.BlogConfig',
]
```

### migrare ad un model nuovo
```
$ python manage.py makemigrations blog
Migrations for 'blog':
  blog/migrations/0001_initial.py
    - Create model Post

$ python manage.py migrate blog
Operations to perform:
  Apply all migrations: blog
Running migrations:
  Applying blog.0001_initial... OK
```

### creazione di uno user
```
$ python manage.py createsuperuser
Username (leave blank to use 'matt'): admin
Email address: admin@admin.admin
Password: 
Password (again): 
Superuser created successfully.
```

## Query set
sarebbe un insieme di oggetti (nel nostro caso i post creati):

```
>>> from blog.models import Post
>>> Post.objects.all()
<QuerySet [<Post: How Django works>, <Post: How ArgoCD works>]>
```

### creare oggetti:
```
>>> from django.contrib.auth.models import User
>>> me = User.objects.get(username='admin')
>>> Post.objects.create(author=me, title='Sample title', text='Test')
<Post: Sample title>
```

### altre funzionalità:

- filtrare: `Post.objects.filter(title__contains='title')`, ...
- ordinare: `Post.objects.order_by('created_date')`, ...

## creazione di pagine dinamiche richiamando codice Python
```html
{% for post in posts %}
  <article>
    <time>published: {{ post.published_date }}</time>
    <h2><a href="">{{ post.title }}</a></h2>
    <p>{{ post.text|linebreaksbr }}</p>        
  </article>
{% endfor %}
```

## identificare id post tramite `pk`
```python
def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    return render(request, 'blog/post_detail.html', {'post': post})
```

